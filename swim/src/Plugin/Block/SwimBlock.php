<?php

namespace Drupal\swim\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SwimBlock' block.
 *
 * @Block(
 *  id = "swim_block",
 *  admin_label = @Translation("Swim block"),
 * )
 */
class SwimBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\swim\Form\StudentForm');

    return $form;
  }

}
